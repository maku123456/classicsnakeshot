﻿using System.Collections.Generic;

[System.Serializable]
public class Levels : ILevels
{
    public List<LevelModel> Level { get ; set ; }

    public void SetData(List<LevelModel> level)
    {
        Level = level;
    }

    public LevelModel GetLevel(int level)
    {
        return Level.Find(x => x.LevelId == level);
    }
}
