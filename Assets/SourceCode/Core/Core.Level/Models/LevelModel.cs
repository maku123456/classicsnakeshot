﻿public class LevelModel : ILevel
{
    public int LevelId { get ; set ; }
    public int Width { get ; set ; }
    public int Height { get ; set ; }
    public long ActiveFoodTime { get ; set ; }
    public float SnakeSpeed { get; set; }
}
