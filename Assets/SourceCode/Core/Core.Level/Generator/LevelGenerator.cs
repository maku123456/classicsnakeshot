﻿using UnityEngine;

namespace Core
{

    /// <summary>
    /// Class takes in the the initial level parameter and generates the level board
    /// </summary>
    public class LevelGenerator : ILevelGenerator
    {
        public ILevel Level { get; set; }
        public IBlockGenerator BlockGenerator { get; set; }
        public IWallGenerator WallGenerator { get; set; }

        public Settings settings;

        public LevelGenerator(IBlockGenerator blockGenerator, IWallGenerator wallGenerator, Settings settings)
        {
            BlockGenerator = blockGenerator;
            WallGenerator = wallGenerator;
            this.settings = settings;
        }

        public void GenerateEnviroment()
        {
            BlockGenerator.GenerateBlocks(Level.Width, Level.Height);
            WallGenerator.GenerateWall(Level.Width, Level.Height);
            GenerateGrounds();
        }

        private void GenerateGrounds()
        {
            for (int i = 0; i < Level.Width; i++)
            {
                for (int j = 0; j < Level.Height; j++)
                {
                    Vector3 pos = new Vector3(i, 0, j);
                    SpawnBlock(i + j, pos);
                }
            }
        }

        private void SpawnBlock(int number, Vector3 pos)
        {
            var go = GameObject.Instantiate(settings.groundPrefab) as GameObject;
            go.transform.SetParent(settings.target);
            go.transform.position = pos;
        }

        [System.Serializable]
        public class Settings
        {
            public GameObject groundPrefab;
            public Transform target;
        }
    }
}