using UnityEngine;
using Zenject;
using Newtonsoft.Json;

namespace Core
{
    /// <summary>
    /// This Class Is reposible providing the instance of the different Level objective 
    /// </summary>
    [CreateAssetMenu(fileName = "LevelSettingsInstaller", menuName = "Installers/LevelSettingsInstaller")]
    public class LevelSettingsInstaller : ScriptableObjectInstaller<LevelSettingsInstaller>
    {
        [SerializeField] private TextAsset levelSnap;

        public override void InstallBindings()
        {
            var json = levelSnap.text;
            Debug.Log("levelJson" + json);
            ILevels levelData = JsonConvert.DeserializeObject<Levels>(json);
            Container.BindInstance(levelData);
        }
    }
}