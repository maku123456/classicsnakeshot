﻿using System;
using UnityEngine;
using Zenject;
namespace Core
{
    /// <summary>
    /// Handles the initial level behaviour this class is runs first sets up the models
    /// </summary>
    public class LevelBehaviour : IInitializable, IDisposable
    {
        ILevelGenerator LevelGenerator;
        IFoodGenerator FoodGenerator;
        ISnakeMovement SnakeMovement;
        ILevels Levels;

        public LevelBehaviour(IFoodGenerator foodGenerator, ISnakeMovement snakeMovement, ILevelGenerator levelGenerator, ILevels levels)
        {
            LevelGenerator = levelGenerator;
            Levels = levels;
            SnakeMovement = snakeMovement;
            FoodGenerator = foodGenerator;
        }

        public void Dispose()
        {

        }

        public void Initialize()
        {
            SetEnviroment();
        }

        private void SetEnviroment()
        {
            if (PlayerPrefs.HasKey("LEVEL"))
            {
                var levelId = PlayerPrefs.GetInt("LEVEL");
                GenerateLevel(levelId);
            }
            else
            {
                GenerateLevel(1);
            }
        }

        private void GenerateLevel(int levelId)
        {
            var level = Levels.GetLevel(levelId);
            LevelGenerator.Level = level;
            LevelGenerator.GenerateEnviroment();
            FoodGenerator.FoodTime = level.ActiveFoodTime;
            SnakeMovement.SnakeSpeed = level.SnakeSpeed;
        }
    }
}