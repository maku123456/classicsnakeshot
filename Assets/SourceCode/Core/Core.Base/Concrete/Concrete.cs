﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public interface IFoodTime
{
    long ActiveFoodTime { get; set; }
}

public interface ILevel: ILevelUnit, ILevelData, IFoodTime
{

}

public interface ILevelGenerator
{
    ILevel Level { get; set; }

    IBlockGenerator BlockGenerator { get; set; }

    IWallGenerator WallGenerator { get; set; }

    void GenerateEnviroment();
}

public interface ILevels
{
    List<LevelModel> Level { get; set; }

    void SetData(List<LevelModel> level);

    LevelModel GetLevel(int level);
}

public interface IPos
{
    int X { get; set; }

    int Y { get; set; }
}

public interface IBlock : IPos
{


    void OnEnterBlockEnter();

    void InitBlock(SignalBus signalBus, Action<IBlock> onHit);

    bool Status { get; set; }
}

public interface IBlockGenerator
{
    List<IBlock> Blocks { get; set; }

    IBlock GetActiveBlock();

    IBlock GetRandomActiveBlock();

    void RemoveBlock(IBlock block);

    void GenerateBlocks(int x, int y);
} 

public interface IWallGenerator
{
    void GenerateWall(int x, int y);
}

public interface IWall
{
    void InitWall(SignalBus signalBus);
}


public interface IMoveable
{
    void Move();
}

public interface ISnake : ISnakeBody, IMoveable
{

}

public interface ISnakeBody
{
    Transform BodyPart { get; }
}

public interface ISnakeMovement
{
    ISnake Snake { get; }

    float SnakeSpeed { get; set; }
}

public interface IFoodGenerator
{
    float FoodTime { get; set; }

    IBlockGenerator BlockGenerator { get; set; }

    void GenearteFood();
}




