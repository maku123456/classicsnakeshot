﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
using Frame.Views;

namespace Core.Views
{
    public class GamePlayView : View<GamePlayView>
    {
        [SerializeField] private Settings settings;

        SignalBus signalBus;

        int score = 0;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        public override void OnEnable()
        {
            base.OnEnable();
            UpdateScore(0);
            signalBus.Subscribe<OnFoodCapturedSignal>(onScoreUpdateSignal);
        }

        public override void OnDisable()
        {
            signalBus.Unsubscribe<OnFoodCapturedSignal>(onScoreUpdateSignal);
            base.OnDisable();
        }

        private void onScoreUpdateSignal(OnFoodCapturedSignal onScoreUpdateSignal)
        {
            Debug.Log("FoodCaptured");
            score++;
            UpdateScore(score);
        }

        private void UpdateScore(int Score)
        {
            settings.txtScore.text = Score.ToString();
        }

        [System.Serializable]
        public class Settings
        {
            public TMP_Text txtScore;
            public TMP_Text txtStrekScore;
        }
    }
}