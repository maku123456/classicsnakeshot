﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core.ScoreSystem
{
    public class ScoreManager : IInitializable, IDisposable, IScoreManager
    {
        private const string HighScoreConst = "HIGHSCORE";

        private int currentScore = 0;

        private int highScore = 0;
        private SignalBus signalBus;

        public int HighScore => highScore;

        public int CurrentScore { get => currentScore; }

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }
        public void Dispose()
        {
            signalBus.Unsubscribe<OnFoodCapturedSignal>(OnFoodHit);
        }

        public void Initialize()
        {
            signalBus.Subscribe<OnFoodCapturedSignal>(OnFoodHit);
            LoadHighScore();
        }

        private void LoadHighScore()
        {
            highScore = (PlayerPrefs.HasKey(HighScoreConst)) ? PlayerPrefs.GetInt(HighScoreConst) : 0;
        }

        public void Reset()
        {
            currentScore = 0;
        }

        private void OnFoodHit(OnFoodCapturedSignal onFoodHitSignal)
        {

            currentScore += 1;
            
            CheckForNewHighScore();
        }

        private void CheckForNewHighScore()
        {
            if (currentScore > highScore)
            {
                highScore = currentScore;
                PlayerPrefs.SetInt(HighScoreConst, highScore);
            }
        }
    }
}