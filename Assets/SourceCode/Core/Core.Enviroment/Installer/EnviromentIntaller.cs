using Core.Views;
using UnityEngine;
using Zenject;

namespace Core
{
    public class EnviromentIntaller : MonoInstaller
    {
        [SerializeField] private LevelGenerator.Settings levelSettings;

        [SerializeField] private BlockGenerator.Settings blockSettings;

        [SerializeField] private WallGenerator.Settings wallSettings;

        public override void InstallBindings()
        {

            Container.Bind<GamePlayView>().FromComponentInHierarchy().AsSingle();
            Container.Bind<GameOverView>().FromComponentInHierarchy().AsSingle();
            Container.BindInterfacesAndSelfTo<EndGameBehaviour>().AsSingle();
            Container.BindInterfacesAndSelfTo<LevelBehaviour>().AsSingle();
            Container.BindInterfacesAndSelfTo<WallBehaviour>().AsSingle();
            Container.Bind<ILevelGenerator>().To<LevelGenerator>().AsSingle();
            Container.Bind<IBlockGenerator>().To<BlockGenerator>().AsSingle();
            Container.Bind<IWallGenerator>().To<WallGenerator>().AsSingle();
            BindInstance();
        }

        private void BindInstance()
        {
            Container.BindInstances(levelSettings);
            Container.BindInstances(blockSettings);
            Container.BindInstances(wallSettings);
        }
    }
}