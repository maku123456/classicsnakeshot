﻿using Zenject;

namespace Core
{
    public class GameSignalInstaller : Installer<GameSignalInstaller>
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<OnBlockHitSignal>();
            Container.DeclareSignal<OnSnakeWallHitSignal>();
            Container.DeclareSignal<OnFoodCapturedSignal>();
            Container.DeclareSignal<OnLevelCommplete>();
        }
    }
}
