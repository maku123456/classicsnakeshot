﻿namespace Core
{
    public struct OnBlockHitSignal
    {
        IBlock block;

        public OnBlockHitSignal(IBlock block)
        {
            this.block = block;
        }
    }
}