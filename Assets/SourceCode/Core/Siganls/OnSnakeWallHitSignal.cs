﻿using UnityEngine;
namespace Core
{
    public struct OnSnakeWallHitSignal
    {
        public IWall wall;

        public Transform transform;

        public OnSnakeWallHitSignal(IWall wall, Transform transform)
        {
            this.wall = wall;
            this.transform = transform;
        }
    }
}