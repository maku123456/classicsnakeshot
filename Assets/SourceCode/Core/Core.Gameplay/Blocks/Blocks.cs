﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core
{
    public class Blocks : MonoBehaviour, IBlock
    {
        public bool Status { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        private SignalBus _signalBus;
        protected Action<IBlock> _onHit;

        public void InitBlock(SignalBus signalBus, Action<IBlock> onHit)
        {
            _signalBus = signalBus;
            _onHit = onHit;
        }

        public void OnEnterBlockEnter()
        {
            this.gameObject.SetActive(false);
            _onHit?.Invoke(this);
            _signalBus?.Fire(new OnBlockHitSignal(this));
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Snake")
            {
                OnEnterBlockEnter();
            }
        }

    }
}
