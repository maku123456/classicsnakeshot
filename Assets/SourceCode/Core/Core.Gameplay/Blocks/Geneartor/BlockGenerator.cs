﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core
{
    /// <summary>
    /// Generartes the eatable blocks and performshandles Blocks data
    /// </summary>
    public class BlockGenerator : IBlockGenerator
    {
        public List<IBlock> Blocks { get; set; }

        public Settings settings;

        public SignalBus signalBus;

        public BlockGenerator(Settings settings, SignalBus signalBus)
        {
            this.settings = settings;
            this.signalBus = signalBus;
            Blocks = new List<IBlock>();
        }

        public void GenerateBlocks(int x, int y)
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Vector3 pos = new Vector3(i, settings.blocksYOffset, j);
                    SpawnBlock(i + j, pos);
                }
            }
        }

        private void SpawnBlock(int number, Vector3 pos)
        {
            var go = GameObject.Instantiate(settings.blockPrefab) as GameObject;
            go.transform.SetParent(settings.target);
            go.transform.position = pos;
            var component = go.GetComponent<Blocks>();
            component.X = (int)pos.x;
            component.Y = (int)pos.z;
            component.Status = true;
            Blocks.Add(component);

            component.InitBlock(signalBus, RemoveBlock);

            if (Blocks.Count <= 0)
                signalBus.Fire(new OnLevelCommplete());
        }

        public IBlock GetActiveBlock()
        {
            return Blocks.Find(x => x.Status == true);
        }

        public void RemoveBlock(IBlock block)
        {
            var _block = Blocks.Find(x => x == block);
            if (_block != null)
            {
                _block.Status = false;
                Blocks.Remove(_block);
            }

            if (Blocks.Count <= 0)
                signalBus.Fire(new OnLevelCommplete());
        }

        public IBlock GetRandomActiveBlock()
        {
            int index = UnityEngine.Random.Range(0, Blocks.Count);
            return Blocks[index];
        }

        [Serializable]
        public class Settings
        {
            public GameObject blockPrefab;
            public Transform target;
            public float blocksYOffset;
        }
    }
}
