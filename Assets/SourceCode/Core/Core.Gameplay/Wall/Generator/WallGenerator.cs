﻿using UnityEngine;
using Zenject;

namespace Core
{
    /// <summary>
    /// Handles the Wall Genration Purpose
    /// </summary>
    public class WallGenerator : IWallGenerator
    {
        public Settings settings;
        public SignalBus signalBus;
        public WallGenerator(Settings settings, SignalBus signalBus)
        {
            this.settings = settings;
            this.signalBus = signalBus;
        }

        public void GenerateWall(int x, int y)
        {
            for (int i = -1; i < x + 1; i++)
            {
                for (int j = -1; j < y + 1; j++)
                {
                    Vector3 pos = new Vector3(i, 1, j);

                    //Spawn the wall at the Outer Edge
                    if (i < 0)
                        SpawnWall(i + j, pos);
                    else if (j < 0)
                        SpawnWall(i + j, pos);
                    else if (i >= x)
                        SpawnWall(i + j, pos);
                    else if (j >= y)
                        SpawnWall(i + j, pos);

                }
            }
        }

        private void SpawnWall(int number, Vector3 pos)
        {
            var go = GameObject.Instantiate(settings.wallPrefab) as GameObject;
            go.transform.SetParent(settings.target);
            go.transform.position = pos;
            var component = go.GetComponent<IWall>();
            component.InitWall(signalBus);
        }

        [System.Serializable]
        public class Settings
        {
            public GameObject wallPrefab;
            public Transform target;
        }
    }
}