﻿using System;
using UnityEngine;
using Zenject;
namespace Core
{
    /// <summary>
    /// Handles the Wall Behaviour OnHit Logic etc
    /// </summary>
    public class WallBehaviour : IInitializable, IDisposable
    {
        private SignalBus _signalBus;
        private SnakeMovementBehaviour _snakeMovement;
        public WallBehaviour(SignalBus signalBus, SnakeMovementBehaviour snakeMovement)
        {
            _signalBus = signalBus;
            _snakeMovement = snakeMovement;
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<OnSnakeWallHitSignal>(OnWallHit);
        }

        public void Initialize()
        {
            _signalBus.Subscribe<OnSnakeWallHitSignal>(OnWallHit);
        }

        private void OnWallHit(OnSnakeWallHitSignal onSnakeWallHitSignal)
        {
            Debug.Log("OnWall Hit behaviour");
            //Bounce the snake back here
            _snakeMovement.BounceBack(onSnakeWallHitSignal.transform);
        }

        [System.Serializable]
        public class Settings
        {
            public GameObject wallPrefab;
        }

    }
}