﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core
{
    public class Wall : MonoBehaviour, IWall
    {
        private SignalBus _signalBus;

        public void InitWall(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Snake")
            {
                _signalBus?.Fire(new OnSnakeWallHitSignal(this, other.gameObject.transform));
            }
        }
    }
}