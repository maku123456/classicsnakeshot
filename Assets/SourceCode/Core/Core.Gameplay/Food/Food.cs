﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core
{
    public interface IFood
    {
        void InitFood(SignalBus signalBus);
    }

    public class Food : MonoBehaviour, IFood
    {
        private SignalBus signalBus;

        public void InitFood(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Snake")
            {
                signalBus.Fire<OnFoodCapturedSignal>();
                this.gameObject.SetActive(false);
            }
        }
    }
}
