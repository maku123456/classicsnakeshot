﻿using System;
using UnityEngine;
using Zenject;
namespace Core
{
    /// <summary>
    /// HOlldes the food spawning And timming paarmeters
    /// </summary>
    public class FoodSpawnBehaviour : IInitializable, ITickable, IDisposable
    {
        public float timeLeft = 3.0f;

        IFoodGenerator FoodGenerator;

        SignalBus signalBus;

        public FoodSpawnBehaviour(IFoodGenerator foodGenerator, SignalBus signalBus)
        {
            FoodGenerator = foodGenerator;
            this.signalBus = signalBus;
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<OnFoodCapturedSignal>(OnFoodCaptured);
        }

        public void Initialize()
        {
            timeLeft = FoodGenerator.FoodTime;
            FoodGenerator.GenearteFood();
            signalBus.Subscribe<OnFoodCapturedSignal>(OnFoodCaptured);
        }

        public void Tick()
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                timeLeft = 30f;
                timeLeft = FoodGenerator.FoodTime;
                FoodGenerator.GenearteFood();
            }
        }

        private void OnFoodCaptured()
        {
            timeLeft = FoodGenerator.FoodTime;
            FoodGenerator.GenearteFood();
        }
    }
}