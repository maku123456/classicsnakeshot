﻿using UnityEngine;
using Zenject;

namespace Core
{
    /// <summary>
    /// Generates the food mand gets the blocks contained and Instanities the Food
    /// </summary>
    public class FoodGenerator : IFoodGenerator
    {
        public float FoodTime { get; set; }
        public IBlockGenerator BlockGenerator { get; set; }

        public Settings settings;

        public SignalBus _signalBus;

        private GameObject CurrentFood;


        public FoodGenerator(SignalBus signalBus, IBlockGenerator blockGenerator, Settings settings)
        {
            BlockGenerator = blockGenerator;
            this.settings = settings;
            _signalBus = signalBus;
        }

        public void GenearteFood()
        {
            var block = BlockGenerator.GetRandomActiveBlock();
            SpawnFood(block);
        }

        private void SpawnFood(IBlock block)
        {
            if (CurrentFood != null)
            {
                GameObject.Destroy(CurrentFood);
            }
            var go = GameObject.Instantiate(settings.foodPrefab) as GameObject;
            var foodSpawnPos = new Vector3(block.X, 2, block.Y);
            go.transform.position = foodSpawnPos;
            CurrentFood = go;
            go.transform.SetParent(settings.target);
            var component = go.GetComponent<IFood>();
            component.InitFood(_signalBus);
        }

        [System.Serializable]
        public class Settings
        {
            public GameObject foodPrefab;
            public Transform target;
        }
    }
}