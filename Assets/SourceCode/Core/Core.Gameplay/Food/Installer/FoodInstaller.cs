using UnityEngine;
using Zenject;
namespace Core
{
    public class FoodInstaller : MonoInstaller
    {
        [SerializeField] private FoodGenerator.Settings foodSettings;
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<FoodSpawnBehaviour>().AsSingle();
            Container.Bind<IFoodGenerator>().To<FoodGenerator>().AsSingle();
            Container.BindInstance(foodSettings);
        }
    }
}