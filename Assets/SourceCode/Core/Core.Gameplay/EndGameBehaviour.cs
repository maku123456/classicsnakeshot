﻿using Core.Views;
using System;
using Zenject;
namespace Core
{
    public class EndGameBehaviour : IInitializable, IDisposable
    {
        GameOverView gameOverView;
        SignalBus signalBus;

        public EndGameBehaviour(GameOverView gameOverView, SignalBus signalBus)
        {
            this.gameOverView = gameOverView;
            this.signalBus = signalBus;
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<OnLevelCommplete>(Init);

        }

        public void Initialize()
        {
            signalBus.Subscribe<OnLevelCommplete>(Init);
        }

        private void Init()
        {
            gameOverView.Open();
        }
    }
}