﻿using UnityEngine;
using Zenject;

namespace Core.Snake
{
    //Instal the instance of the snake parameter here
    public class SnakeInstaller : MonoInstaller<SnakeInstaller>
    {
        [SerializeField] private SnakeSettings snakeSettings;
        public override void InstallBindings()
        {
            Container.BindInstance(snakeSettings);
            Container.Bind<ISnakeMovement>().To<SnakeMovementRig>().AsSingle();
            Container.BindInterfacesAndSelfTo<SnakeMovementBehaviour>().AsSingle();
        }
    }
}