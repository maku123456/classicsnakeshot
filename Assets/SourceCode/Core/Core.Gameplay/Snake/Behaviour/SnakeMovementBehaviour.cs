﻿
using Core.Snake;
using UnityEngine;
using Zenject;

namespace Core
{
    /// <summary>
    /// Handles Snake Movement Behaviouralso the Bounce effect
    /// </summary>
    public class SnakeMovementBehaviour : ITickable
    {
        private ISnakeMovement SnakeMovement;

        public SnakeSettings settings;

        public SnakeMovementBehaviour(ISnakeMovement SnakeMovement, SnakeSettings settings)
        {
            this.settings = settings;
            this.SnakeMovement = SnakeMovement;
        }

        public void Tick()
        {
            MoveSnake();
        }

        private void MoveSnake()
        {
            //FIXME: Use command pattern here
            if (settings.steeringJoyStick.Horizontal != 0 )
                settings.head.Rotate(Vector3.up * settings.rotationSpeed * Time.deltaTime * settings.steeringJoyStick.Horizontal);
            else if(Input.GetAxis("Horizontal") != 0)
                settings.head.Rotate(Vector3.up * settings.rotationSpeed * Time.deltaTime * Input.GetAxis("Horizontal"));

            settings.head.Translate(settings.head.forward * SnakeMovement.SnakeSpeed * Time.smoothDeltaTime, Space.World);
        }

        public void BounceBack(Transform trafsm)
        {
            //TODO:Perform the Bounce back effect here

            Vector3 reflectposition = -2 * (Vector3.Dot(settings.head.position, trafsm.position) * trafsm.position - settings.head.position);
            reflectposition.y = settings.head.position.y;
            settings.head.LookAt(reflectposition);
        }
    }
}