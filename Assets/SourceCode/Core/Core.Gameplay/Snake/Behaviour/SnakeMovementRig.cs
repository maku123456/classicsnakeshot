﻿namespace Core
{
    /// <summary>
    /// Holds the Snake Movement details and the speed factor
    /// </summary>
    public class SnakeMovementRig : ISnakeMovement
    {
        public ISnake Snake { get; private set; }
        public float SnakeSpeed { get; set; }
    }
}
