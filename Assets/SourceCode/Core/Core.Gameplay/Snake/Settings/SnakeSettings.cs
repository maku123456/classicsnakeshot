﻿using System.Collections.Generic;
using UnityEngine;
namespace Core.Snake
{
    [System.Serializable]
    public class SnakeSettings
    {
        public Transform head;
        [Space]
        public float minDistance = 1f;
        public float speed = 1f;
        public float rotationSpeed = 50f;
        public GameObject snakeBody;
        public int beginSize = 1;

        public Rigidbody rigidbody;

        public SteeringJoyStick steeringJoyStick;
    }
}
