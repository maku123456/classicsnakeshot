{
  "level": [
    {
      "levelId": 1,
      "width": 50,
      "height": 50,
      "activeFoodTime": 10,
      "snakeSpeed": 10.0
    },
    {
      "levelId": 2,
      "width": 6,
      "height": 6,
      "activeFoodTime": 15,
      "snakeSpeed": 10.0
    },
    {
      "levelId": 3,
      "width": 7,
      "height": 5,
      "activeFoodTime": 10,
      "snakeSpeed": 10.0
    },
    {
      "levelId": 4,
      "width": 7,
      "height": 7,
      "activeFoodTime": 5,
      "snakeSpeed": 10.0
    }
  ]
}