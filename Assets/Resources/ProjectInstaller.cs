using Core.ScoreSystem;
using Frame.Views;
using UnityEngine;
using Zenject;

namespace Core
{
    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            GameSignalInstaller.Install(Container);
            ViewInstaller.Install(Container);
            Container.BindExecutionOrder<ScoreManager>(-100);
            Container.BindInterfacesAndSelfTo<ScoreManager>().AsSingle().NonLazy();
        }
    }
}