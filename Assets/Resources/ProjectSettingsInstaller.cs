using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ProjectSettingsInstaller", menuName = "Installers/ProjectSettingsInstaller")]
public class ProjectSettingsInstaller : ScriptableObjectInstaller<ProjectSettingsInstaller>
{
    public override void InstallBindings()
    {
    }
}